import 'package:bloc_overview/bloc/counter_bloc.dart';
import 'package:bloc_overview/bloc/image_picker_bloc/image_picker_bloc.dart';
import 'package:bloc_overview/bloc/switch_bloc/switch_bloc.dart';
import 'package:bloc_overview/bloc/todo_bloc/todo_bloc.dart';
import 'package:bloc_overview/ui/home_page.dart';
import 'package:bloc_overview/utils/image_picker_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<CounterBloc>(create: (context) => CounterBloc(),),
        BlocProvider<SwitchBloc>(create: (context) => SwitchBloc(),),
        BlocProvider<ImagePickerBloc>(create: (context) => ImagePickerBloc(ImagePickerUtils()),),
        BlocProvider<TodoBloc>(create: (context) => TodoBloc()),
      ],

      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
        ),
        home: HomePage(),
      ),
    );
  }
}
