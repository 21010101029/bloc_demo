import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'todo_event.dart';
part 'todo_state.dart';

class TodoBloc extends Bloc<TodoEvent, TodoState> {
  final List<String> todos = [];
  TodoBloc() : super(const TodoState()) {
    on<AddTodoEvent>(_addToEvent);
    on<RemoveTodoEvent>(_removeEvent);
  }

  void _addToEvent(AddTodoEvent event, Emitter<TodoState> emit){
    todos.add(event.task);
    emit(state.copyWith(list: List.from(todos) ));
  }

  void _removeEvent(RemoveTodoEvent event, Emitter<TodoState> emit){
    todos.remove(event.index);
    emit(state.copyWith(list: List.from(todos)  ));
  }
}
