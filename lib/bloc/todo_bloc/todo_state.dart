part of 'todo_bloc.dart';

class TodoState extends Equatable{

  final List<String> todos;
  const TodoState({this.todos=const []});


  TodoState copyWith({List<String>? list}){
    return TodoState(todos: list?? todos);
  }
  @override
  // TODO: implement props
  List<Object?> get props => [todos];

}
