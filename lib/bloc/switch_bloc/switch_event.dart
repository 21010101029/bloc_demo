part of 'switch_bloc.dart';

abstract class SwitchEvents extends Equatable{
  SwitchEvents();
  @override
  List<Object?> get props=>[];
}

class EnableOrDisableNotification extends SwitchEvents{}
class SliderEvent  extends SwitchEvents{
  double ?value;
  SliderEvent({required val}){
    this.value=val;
  }
  @override
  List<Object?> get props=>[value];

}
