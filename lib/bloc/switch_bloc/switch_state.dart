part of 'switch_bloc.dart';

class SwitchState extends Equatable {
  final bool isSwitch;
  final double sliderValue;

  SwitchState({this.isSwitch = false, this.sliderValue = 0.0});

  SwitchState copyWith({bool? isSwitch,double? sliderValue }) {
    return SwitchState(
        isSwitch: isSwitch ?? this.isSwitch,
        sliderValue: sliderValue ?? this.sliderValue);
  }

  @override
  List<Object?> get props => [isSwitch,sliderValue];
}
