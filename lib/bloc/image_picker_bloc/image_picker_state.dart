part of 'image_picker_bloc.dart';


final class ImagePickerState extends Equatable {

  final XFile? file;

  ImagePickerState({this.file});

  ImagePickerState copyWith({XFile? file}){
    return ImagePickerState(file: file);
  }

  @override
  // TODO: implement props
  List<Object?> get props => [file];
}
