import 'package:bloc_overview/bloc/switch_bloc/switch_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SwitchDemo extends StatefulWidget {
  const SwitchDemo({super.key});

  @override
  State<SwitchDemo> createState() => _SwitchDemoState();
}

class _SwitchDemoState extends State<SwitchDemo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Switch Demo"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                BlocBuilder<SwitchBloc,SwitchState>(
                  buildWhen: (previous, current) => previous.isSwitch!=current.isSwitch,
                  builder: (context, state) {
                    print("object");
                    return Switch(
                      value: state.isSwitch,
                      onChanged: (value) {
                        context.read<SwitchBloc>().add(EnableOrDisableNotification());
                      },
                    );
                  },
                )
              ],
            ),
            BlocBuilder<SwitchBloc,SwitchState>(
              buildWhen: (previous, current) => previous.sliderValue!=current.sliderValue,
              builder: (context, state) {
                // print("Slider");
                return   Container(
                  height: 200,
                  color: Colors.redAccent.withOpacity(state.sliderValue),
                );
              },

            ),
            BlocBuilder<SwitchBloc,SwitchState>(
              
               builder: (context, state) {
                 // print("Slider22");
                 return Slider(
                   value: state.sliderValue,
                   onChanged: (value) {
                     context.read<SwitchBloc>().add(SliderEvent(val: value));
                   },
                 );
               },
            ),
          ],
        ),
      ),
    );
  }
}
