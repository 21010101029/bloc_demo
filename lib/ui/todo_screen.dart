import 'package:bloc_overview/bloc/todo_bloc/todo_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ToDoScreen extends StatefulWidget {
  const ToDoScreen({super.key});

  @override
  State<ToDoScreen> createState() => _ToDoScreenState();
}

class _ToDoScreenState extends State<ToDoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Todo Exmample"),
      ),
      body: Column(children: [
        Expanded(
          child: BlocBuilder<TodoBloc, TodoState>(
            builder: (context, state) {
              if(state.todos.isEmpty){
                return Center(child: Text("No Data"),);
              }else if(state.todos.isNotEmpty) {
                return ListView.builder(
                  itemCount: state.todos.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      title: Text(state.todos[index]),
                      trailing: GestureDetector(
                          onTap: () {
                            BlocProvider.of<TodoBloc>(context).add(
                                RemoveTodoEvent(state.todos[index]));
                          }, child: const Icon(Icons.delete_outline)),
                    );
                  },
                );
              }else{
                return SizedBox();
              }
            },
          ),
        )
      ]),
      floatingActionButton: FloatingActionButton(
        onPressed: ()  {
          for(int i = 0 ; i < 10 ; i++ ){
            BlocProvider.of<TodoBloc>(context).add(AddTodoEvent('Task $i'));
          }
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
