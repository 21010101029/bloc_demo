import 'package:bloc_overview/ui/counter_screen.dart';
import 'package:bloc_overview/ui/image_picker_screen.dart';
import 'package:bloc_overview/ui/switch_screen.dart';
import 'package:bloc_overview/ui/todo_screen.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Bloc Examples "),
      ),
      body: Column(
        children: [
          Card(
            child: ListTile(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => CounterScreen(),));
              },
              title: Text("CounterDemo"),
            ),
          ),
          Card(
            child: ListTile(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => SwitchDemo(),));
              },
              title: Text("Switch Demo"),
            ),
          ),
          Card(
            child: ListTile(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => ImagePickerScreen(),));
              },
              title: Text("Image Picker  Demo"),
            ),
          ),
          Card(
            child: ListTile(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => ToDoScreen(),));
              },
              title: Text("TODO Demo"),
            ),
          ),
        ],
      ),
    );
  }
}
